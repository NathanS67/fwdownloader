<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
?>

<?php require_once("../header.html"); ?>
<main>
    <?php
        $JSON = json_decode(file_get_contents("https://api.bretzel.ga/funkwhale-api/api"), true);
        echo "<ul>\n";
        foreach ($JSON as $artiste) {
            echo "\t\t<li>" . $artiste["artiste"] . "</li>\n";
            echo "\t\t<ul>\n";
            foreach ($artiste["albums"] as $album) {
                echo "\t\t\t<li>" . $album["titre"] . "</li>\n";
                echo "\t\t\t<ul>\n";
                foreach ($album["pistes"] as $piste) {
                    echo "\t\t\t\t<li><a href=\"https://zik.bretzel.ga" . $piste["url"] . "\">" . $piste["titre"] . "</a></li>\n";
                }
                echo "\t\t\t</ul>\n";
            }
            echo "\t\t</ul>\n";
        }
        echo "\t</ul>\n";
    ?>
</main>
<?php require_once("../footer.html");
