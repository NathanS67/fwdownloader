const fetch = require("node-fetch");
const express = require("express");

let def_url = "https://zik.bretzel.ga/api/v1/tracks/";
let next = def_url;
let jsonret = [];

const update_array = async () => {
    let array = [];
    while (next != null) {
        await fetch(next, { method: "GET" })
            .then(response => {
                return response.json()
            })
            .then(json => {
                next = json["next"];
                array.push(json["results"]);
            })
            .catch(err => {
                console.error(err);
            });
    }
    let sorted = []
    array.forEach(page => {
        page.forEach(musique => {
            let i = sorted.map(function (e) { return e["artiste"]; }).indexOf(musique["artist"]["name"]);
            let piste = {
                "titre": musique["title"],
                "url": musique["listen_url"],
                "position": musique["position"]
            };
            let album = {
                "titre": musique["album"]["title"],
                "pistes": [piste]
            };
            let artiste = {
                "artiste": musique["artist"]["name"],
                "albums": [
                    album
                ]
            };
            if (i == -1) {
                sorted.push(artiste);
            }
            else {
                let j = sorted[i]["albums"].map(function (e) { return e["titre"]; }).indexOf(musique["album"]["title"]);
                if (j == -1) {
                    sorted[i]["albums"].push(album);
                }
                else {
                    sorted[i]["albums"][j]["pistes"].push(piste);
                }
            }
        });
    });
    sorted.sort((a, b) => (a["artiste"] > b["artiste"]) ? 1 : -1);
    sorted.forEach(artist => {
        artist["albums"].sort((a, b) => (a["titre"] > b["titre"]) ? 1 : -1);
        artist["albums"].forEach(album => {
            album["pistes"].sort((a, b) => (a["position"] > b["position"]) ? 1 : -1);
        });
    });
    jsonret = sorted;
    next = def_url;
    setTimeout(update_array, 60000);
}

update_array();

let app = express();

app.use((req, res, next) => {
    next();
});

app.get("/funkwhale-api/api/", (req, res) => {
    res.json(jsonret);
});

app.listen(6767);